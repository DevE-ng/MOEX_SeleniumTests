# Test Framework Components: Java 1.8 + Maven + TestNg + Allure

## Steps to execute tests:

*  Download zip, unpack, 
*  In command prompt open project location and run command "mvn clean test"
*  After the test execution is finished, run command "mvn allure:serve" - The report will be opened in your local browser

## PREREQUISITES
*   Java 1.8
*   Maven 3.3.9
*   Google chrome current version 76(make sure you have latest 76) should be used with the corresponding latest chrome driver version 76(already available in the project directory)

## NOTE:
* Selenium tests has been implemented with PageObject design pattern
* The test called "test_getDealsForToday" is implemented such that it fails if there are no records found in the table
* Additional test called "test_getDealsLastMonth" has been written since no records were found for the desired test "test_getDealsForToday"
* The csv file 'list_deals' is saved inside the /MOEX_SeleniumTests-master folder
* The file saving can fail if user's internet connection is slow. In that case, "Thread.sleep(2000)" value could be increased inside the saveFile() method in the class UserHomePage
